//
//  EditProfileVC.swift
//  FirebaseAppDemo
//
//  Created by Kapil Dhawan on 06/02/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class EditProfileVC: UIViewController {

    @IBOutlet weak var vwEditName: UIView!
    @IBOutlet weak var vwEditContact: UIView!
    @IBOutlet weak var vwEditInterest: UIView!
    @IBOutlet weak var vwEditAddress: UIView!
    
    @IBOutlet weak var tfEditName: UITextField!
    
    @IBOutlet weak var tfEditInterest: UITextField!
    @IBOutlet weak var tfEditContact: UITextField!
    
    @IBOutlet weak var tfEditAddress: UITextField!
    
    @IBOutlet weak var btnUpdate: UIButton!

    var reference: DatabaseReference!
    var nameToBeUpdated = String()
    var key = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addshadow(component: vwEditName)
        addshadow(component: vwEditContact)
        addshadow(component: vwEditInterest)
        addshadow(component: vwEditAddress)
        addshadow(component: btnUpdate)
        
        let idtosearch = nameToBeUpdated
        let ref = Database.database().reference().child("users")
        ref.queryOrdered(byChild: "name").queryEqual(toValue: idtosearch).observeSingleEvent(of: .childAdded, with: { (snapshot) in
           self.key = "\(snapshot.key)"
        })

    }
    
    @IBAction func btnUpdateClicked(_ sender: Any) {
            print(self.key)
        if self.tfEditAddress.isEditing{
            let value = ["Address": self.tfEditAddress.text!]
            let reference = Database.database().reference().root.child("users").child(self.key).updateChildValues(value)
        }
        if self.tfEditName.isEditing{
            let value = ["name": self.tfEditName.text!]
            let reference = Database.database().reference().root.child("users").child(self.key).updateChildValues(value)
            }
        if self.tfEditContact.isEditing{
            let value = ["Contact": self.tfEditContact.text!]
            let reference = Database.database().reference().root.child("users").child(self.key).updateChildValues(value)
        }
        if self.tfEditInterest.isEditing{
            let value = ["Interest": self.tfEditInterest.text!]
            let reference = Database.database().reference().root.child("users").child(self.key).updateChildValues(value)
        }
            
            print("Data updated")
            
            self.tfEditName.text! = ""
            self.tfEditContact.text! = ""
            self.tfEditInterest.text! = ""
            self.tfEditAddress.text! = ""
            
            self.dismiss(animated: true, completion: nil)
    
         
    }
    
    
    
    func addshadow(component:UIView)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }
    func addshadow(component:UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }

}
  /*  if (self.tfEditName.text!.isEmpty || self.tfEditContact.text!.isEmpty || self.tfEditInterest.text!.isEmpty || self.tfEditAddress.text!.isEmpty) {
    
    let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter data required", preferredStyle: .alert)
    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    
    alertController.addAction(defaultAction)
    self.present(alertController, animated: true, completion: nil)
}
*/
