//
//  LoginVC.swift
//  FirebaseAppDemo
//
//  Created by Kapil Dhawan on 06/02/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginVC: UIViewController {

    @IBOutlet weak var vwUsername: UIView!
    @IBOutlet weak var vwPassword: UIView!
    
    @IBOutlet weak var tfLogUsername: UITextField!
    
    @IBOutlet weak var tfLogPassword: UITextField!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var btnSignup: UIButton!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addshadow(component: vwUsername)
        addshadow(component: vwPassword)
        addshadow(component: btnLogin)
       navigationController?.navigationBar.tintColor = .white
       navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
       navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    

    @IBAction func btnLoginClicked(_ sender: Any) {
        if (self.tfLogPassword.text!.isEmpty || self.tfLogUsername.text!.isEmpty) {
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter data required", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            Auth.auth().signIn(withEmail: tfLogUsername.text!, password: tfLogPassword.text!) { (user, error) in
                if error == nil{
                    print("access granted")
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let data = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                     data.email = self.tfLogUsername.text!
                    
                    data.navController = self.navigationController
                    
                   
                    self.navigationController?.present(data, animated: true)
                   
                    self.tfLogUsername.text! = ""
                    self.tfLogPassword.text! = ""
                }
                else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                    
                    self.tfLogUsername.text! = ""
                    self.tfLogPassword.text! = ""
                }
            }
        }
    }
    
    
    
    
    @IBAction func btnSignupClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        
        self.navigationController?.present(data, animated: true)
    }
    
    
    
    func addshadow(component:UIView)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }
    func addshadow(component:UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }

}
