//
//  SignupVC.swift
//  FirebaseAppDemo
//
//  Created by Kapil Dhawan on 06/02/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignupVC: UIViewController {

   
    @IBOutlet weak var vwUser: UIView!
    @IBOutlet weak var vwConPass: UIView!
    @IBOutlet weak var vwPass: UIView!
    
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    
    @IBOutlet weak var btnSignup: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addshadow(component: vwUser)
        addshadow(component: vwPass)
        addshadow(component: vwConPass)
        addshadow(component: btnSignup)

        
    }
    
    @IBAction func btnSignupClicked(_ sender: Any) {
        if (self.tfUsername.text!.isEmpty || self.tfPassword.text!.isEmpty || self.tfConfirmPassword.text!.isEmpty){
            
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter data required", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
            
        else if tfPassword.text != tfConfirmPassword.text {
            let alertController = UIAlertController(title: "Password Incorrect", message: "Please re-type password", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else{
            Auth.auth().createUser(withEmail: self.tfUsername.text!, password: self.tfPassword.text!) { (user, error) in
                if error == nil{
                    print("added")
                  self.dismiss(animated: true, completion: nil)
                    self.tfUsername.text! = ""
                    self.tfPassword.text! = ""
                    self.tfConfirmPassword.text! = ""
                    
                }
                else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    func addshadow(component:UIView)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }
    func addshadow(component:UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }
}
    
 


