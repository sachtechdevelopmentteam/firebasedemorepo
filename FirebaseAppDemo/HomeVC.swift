//
//  HomeVC.swift
//  FirebaseAppDemo
//
//  Created by Kapil Dhawan on 06/02/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth

class HomeVC: UIViewController {
    
     @IBOutlet weak var btnSignout: UIButton!
    
    @IBOutlet weak var btnEditUser: UIButton!
    
    @IBOutlet weak var vwAddress: UIView!
    @IBOutlet weak var vwContact: UIView!
    @IBOutlet weak var vwName: UIView!
    @IBOutlet weak var vwInterest: UIView!
    
    
    @IBOutlet weak var btnDeleteUser: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfContact: UITextField!
    @IBOutlet weak var tfAddress: UITextField!
    @IBOutlet weak var tfInterest: UITextField!
    
    var email = String()
    var ref: DatabaseReference!
    var navController:UINavigationController!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
          ref = Database.database().reference().child("users")
        addshadow(component: vwAddress)
        addshadow(component: vwName)
        addshadow(component: vwContact)
        addshadow(component: vwInterest)
        addshadow(component: btnSubmit)
       
        
    }
    
    @IBAction func btnSubmitClicked(_ sender: Any) {
        if (self.tfName.text!.isEmpty || self.tfContact.text!.isEmpty || self.tfInterest.text!.isEmpty || self.tfAddress.text!.isEmpty) {
            let alertController = UIAlertController(title: "Empty Fields", message: "Please Enter data required", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            let key = ref.childByAutoId().key
            let user = [
                "name" : self.tfName.text!,
                "Contact" : self.tfContact.text!,
                "Interest" : self.tfInterest.text!,
                "Address" : self.tfAddress.text!
            ]
            ref.child(key!).setValue(user)
            print("Data added")
            self.tfName.text! = ""
            self.tfContact.text! = ""
            self.tfInterest.text! = ""
            self.tfAddress.text! = ""
            
        }
    }
    @IBAction func btnSignoutClicked(_ sender: Any) {
    do {
            try Auth.auth().signOut()
        }
        catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        self.dismiss(animated: true, completion: nil)
         }
    
    
    @IBAction func btnEditClicked(_ sender: Any) {
    
    }
    @IBAction func btnEditProfile(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "UpdateVC") as! UpdateVC
        self.present(data, animated: true, completion: nil)
    }
    
    
    
    @IBAction func btnDeleteClicked(_ sender: Any) {
        let user = Auth.auth().currentUser
        
        user?.delete { error in
            if let error = error {
                print("error")
            } else {
                print("Account deleted.")
                self.dismiss(animated: true, completion: nil)
                
            }
        }
    }
    
    func addshadow(component:UIView)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }
    func addshadow(component:UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }
}
