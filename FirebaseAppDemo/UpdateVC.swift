//
//  UpdateVC.swift
//  FirebaseAppDemo
//
//  Created by Kapil Dhawan on 06/02/19.
//  Copyright © 2019 Kapil Dhawan. All rights reserved.
//

import UIKit

class UpdateVC: UIViewController {

    @IBOutlet weak var tfUpdateName: UITextField!
   
    
    @IBOutlet weak var btnEdit: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        addshadow(component: btnEdit)

        
    }
    

    @IBAction func btnEditClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let data = storyboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        data.nameToBeUpdated = self.tfUpdateName.text!
       
        self.present(data, animated: true, completion: nil)
        
        
    }
    
    @IBAction func btnHomeTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    func addshadow(component:UIButton)
    {
        component.layer.shadowColor = UIColor.black.cgColor
        component.layer.shadowOffset = CGSize(width: 5, height: 5)
        component.layer.shadowRadius = 5
        component.layer.shadowOpacity = 1.0
    }

}
